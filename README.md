# WordViz #

This is is RubyOnRails web application that creates a "wordmap" of Yelp reviews.

### What is this repository for? ###

* A wordmap quickly tells you what a restaurant or bar is popular for. This is especially useful when you visit a new city.
* Just by reading the name of a restaurant, one cannot figure out the speciality of that restaurant. Wordmap clearly shows you that.
* Uses real Yelp reviews from the Academic dataset

See screenshots below

![WordViz1.png](https://bitbucket.org/repo/BbeLrR/images/1876538796-WordViz1.png)

![Wordviz2.png](https://bitbucket.org/repo/BbeLrR/images/640332975-Wordviz2.png)