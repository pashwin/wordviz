class BusinessData < ActiveRecord::Base
  attr_accessible :business_id, :categories, :city, :full_address, :name, :neighborhoods, :open, :photo_url, :review_count, :schools, :stars, :state, :type, :url
  serialize :neighborhoods
  serialize :categories
  serialize :schools
end
