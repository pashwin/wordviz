class WordCloud < ActiveRecord::Base
  attr_accessible :business_id, :word_count, :word_reviewids
  serialize :word_count
  serialize :word_reviewids
end
