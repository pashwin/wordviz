class UserData < ActiveRecord::Base
  attr_accessible :average_stars, :name, :review_count, :type, :url, :user_id
end
