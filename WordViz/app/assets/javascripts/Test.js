var colorsData
function testClicked() {
	
	
	alert("Clicked successfully")
	makeFakeCall()
}

function makeFakeCall() {
	$.ajax({
		beforeSend: function(xhr) {
		    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
		  },
		type : "POST",
		url : "/load_db/fakecall",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		success : function(result) {
			console.log("done");
		}

	});

}


function makeFakeCallUser() {
	
	$.ajax({
		beforeSend: function(xhr) {
		    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
		  },
		type : "POST",
		url : "/load_db/fakecallUser",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		success : function(result) {
			console.log("done");

		}

	});

}



function loadRestaurants() {
	
/* 	
	$.ajax({
		beforeSend: function(xhr) {
		    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
		  },
		type : "POST",
 
		url : "/load_db/loadrestaurants",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		success : function(result) {
			console.log(result);
			
			populateRestaurants(result)
 
		}

	});
 	
	
*/	
	
}


function populateRestaurants(restaurants) {
	
	console.log(restaurants)
	$("#maincontent").append(restaurants)
			
	
	
	
	
	
 
}


$(document).ready(function() {
	
	//$(document).ready(function(){
	  //  $("#container_1 div").click(function(){ console.log(this.id);});
	//});
	//loadRestaurants()
	loadWordClouds()
	
	
	
});




function makeFakeCallBusiness() {
	
	$.ajax({
		beforeSend: function(xhr) {
		    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
		  },
		type : "POST",

		url : "/load_db/fakecallBusiness",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		success : function(result) {
			console.log("done");

		}

	});

}	


function loadWordClouds() {
	data = getDataForAllClouds()
	
	
	
}
//Need to merge this one and the next function
function getDataForSingleRestaurant(businessId, divId) {
	
	//alert("in getDataForSingleRestaurant");
	//alert(businessId);
	
	business_ids = new Array()
	
	business_ids.push(businessId)
	
	var ids_in_json = JSON.stringify(business_ids)
	$.ajax({
		beforeSend: function(xhr) {
		    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
		  },
		type : "POST",
		data: "business_ids="+ids_in_json,
		dataType : "json",
		url : "/load_db/getWordCloudData",
		success : function(result) {
			console.log(result);
			
			displayWordCloud(result[businessId], divId)
			
		}

	});
}

function getDataForAllClouds() {
	
	var ids = $('[id^="restaurant_"]')
	
	var business_ids = new Array()
	
	for ( var i =0; i < ids.length; i++) {
		
		var split_ids = ids[i].id.split("t__")
		//alert(split_ids[1])
		business_ids.push(split_ids[1])
	}
	
	var ids_in_json = JSON.stringify(business_ids)
	console.log(ids_in_json)
	$.ajax({
		beforeSend: function(xhr) {
		    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
		  },
		type : "POST",
		data: "business_ids="+ids_in_json,
		dataType : "json",
		url : "/load_db/getWordCloudData",
		success : function(result) {
			console.log(result);
			
			var wcloudIds = $('[id^="wcloud_"]')
			
			for (var i =0; i < wcloudIds.length; i++ ) {
				
				var businessId = wcloudIds[i].id.split('d__')[1]
				displayWordCloud(result[businessId], "wcloud__"+businessId)
				
			}
		}

	});
	
}

 

function fetchData()
{
	$.ajax({
		beforeSend: function(xhr) {
		    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
		  },
		type : "POST",

		url : "/load_db/fetchData",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		success : function(result) {
			console.log("done");
		}

	});

}

var x = 1
var y = 1
function displayWordCloud(data, div_id) {
	
	
	var wordcloudData = data//[0]
	
	var keys = Object.keys(wordcloudData);
	
	d3.layout.cloud().size([300, 300])
    .words(keys.map(function(d) {
            			return {
            					text: d, 
            					size: wordcloudData[d]['frequency']
            					
            			};
            	}))
    .rotate(function() {
    	
    	var rot =  ~~(Math.random() * x) * y;
    	x = 2
    	y = 90
    	
    	return rot
    })
    .fontSize(function(d) { return d.size; })
    .on("end", draw)
    .start();

	function draw(words) {
			
			
		d3.select("#"+div_id).append("svg")
	          .attr("width" , 300)
	          .attr("height", 300)
	          .append("g")
	          .attr("transform", "translate(150,150)")
	          .selectAll("text")
	          .data(words)
	          .enter().append("text")
	          .style("font-size", function(d) { return d.size + "px"; })
	           
	          .style("fill", function(d) {
	    	
	        	  		var color = wordcloudData[d.text]['color']
	    	  
        	  			return color;
	      
	          })
	      
	          .on("mouseover" , function(d){ 
	        	  d3.select(this).style(
	        			  //'background-color','green'
	        			 // 'border', function(d) { return "1px solid yellow"; }
	        			  "font-size", function(d) { return d.size + 5 +  "px"; }
	        			  )
	        	  
	          })
	          
	             .on("mouseout" , function(d){ 
	        	  d3.select(this).style(
	        			  //'background-color','green'
	        			 // 'border', function(d) { return "1px solid yellow"; }
	        			  "font-size", function(d) { return d.size - 5 +  "px"; }
	        			  )
	        	  
	          })
	          .attr("text-anchor", "middle")
	          .on("click", function(d) {   
	        	  		var dd  = d
        	  			//alert(d)
//	        	  		alert(d.text)
	        	  		fetchReviewsContainingWord(d.text, business_id) 
	          })
	          .attr("transform", function(d) {
	        
	        	  		return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
	          })
	          .text(function(d) { return d.text; });
	}
}
 

function showReviews()
{
	 
	 //alert("clicked");
	 //window.location = "/wordViz/reviews"
    //getDataForSingleRestaurant("7zDbcbUkafhkanpTE5TQRQ", "wcloud__1")
}

function showrestaurantspopup(e) {
	$("#dropbown").css("visibility", "visible");
	$("#dropbown").css("height", "500px");
	$("#dropbown").css("width", "500px");
	$("#dropbown").show();
	$("#dropbown").dialog( {
		height:500,
		width :550,
		modal:true
	});
	//$('[id^="#combo_"]').ellipsis();
	//$("#specialities").ellipsis();
	//alert("")
	$(".ui-dialog-titlebar").hide() 
	$(document).on('click', '.ui-widget-overlay', function() {
    $('.ui-dialog-content').each(function() {
       $(this).dialog('close'); 
    });
    
    
});
	//e.preventDefault();
	return false;
}


function showreviewspage(business_id) {
	
	
	window.location = "/wordViz/reviews?id="+business_id;
}