//var business_id = "GJIZlVlPncUQk9x1EjgnFA"
var business_id = ""	
var reviewsContainerWidthOriginal = 0

$(document).ready(function() {
	 
	 
        var layout = $('body').layout({
            applyDemoStyles : true
        });
        
        layout.sizePane("west",350);
        $(".ui-layout-center").css("padding", "0px");
        
        $(".ui-layout-west").css("background-color", "#5F0000");
        

    business_id = $('div[id^="xxx"]').attr("id");
        
        var n=business_id.split("__");

	    business_id=n[1];

        getDataForSingleRestaurant(business_id, "wcloud__1")

        fetchReviewsContainingWord("", business_id)
        
       
        
        addHoverListeners()
 
 
 }); 

 
 
 function fetchReviewsContainingWord(word, businessid) {
	 
	 
	 
	 $.ajax({
			beforeSend: function(xhr) {
			    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
			  },
			type : "POST",
			data: "word="+word+"&businessid="+businessid,
			dataType : "json",
			url : "/load_db/fetchReviews",
			success : function(result) {
				console.log(result);
				addReviewDivs(result)
				
			}

		});
	 
	 
 }
 
 function hideDetailsOnHover() {
	 console.log("hidden")
 }
 
 function showDetailsOnHover() {
	 console.log("Hovered")
	 
	 var detailsdiv = $("<div style='border:1px solid red;height:100px; width:100px;' id='tooltip' class='tooltip_x'>I'm the tooltip!</div>");
	   
	    $(this).mousemove(function(event){
	    	
	    	$('body').append(detailsdiv)
	        $('.tooltip_x').css({'top':(event.pageY - 325),'left':(event.pageX - 325)});
	    });
	 	     	 
 }

 function addReviewDivs(reviewData) {
	 
	 var widthInFiveStar = 0
	 var widthInFourStar = 0
	 var widthInBadStar  = 0
	 var reviewsContainerWidth = 0
	 
	 if( reviewsContainerWidthOriginal >  0 ) {
		 $("#reviewcontainer").width(reviewsContainerWidthOriginal)	 
	 } else {
		 reviewsContainerWidthOriginal = $("#reviewcontainer").width()
			 
	 }
	 
	 $("#fivestarreviewpanel").empty()
	 $("#fourstarreviewpanel").empty()
	 $("#badreviewspanel").empty()
	for (var i =0; i < reviewData.length; i++) {
		
		reviewsContainerWidth = $("#reviewcontainer").width()
		
		console.log(reviewData[i])
		
		review 		= reviewData[i]
		var stars  = review.star

		var text   = review.review // CHANGE THIS -- DONT USE ANOTHER PARAM
		
		
		if(stars == 5) {
			
			widthInFiveStar += 300
			
			if(widthInFiveStar + 300 > reviewsContainerWidth ) {
				$("#reviewcontainer").width(reviewsContainerWidth + 300)
			}
			
			html = "<div class='reviewpanel'>    " +
			 text + 
			"</div>"
	   		
	   		$("#fivestarreviewpanel").append(html)
	   
		} else if(stars == 4) {
			
			widthInFourStar += 300
			
			if(widthInFourStar + 300 > reviewsContainerWidth ) {
				$("#reviewcontainer").width(reviewsContainerWidth + 300)
			}


			
			html = "<div class='reviewpanel'>    " +
			   		text +
			   		"</div>"

			$("#fourstarreviewpanel").append(html)
			
			
		}else {
			widthInBadStar += 300
			
			if(widthInBadStar + 300 > reviewsContainerWidth ) {
				$("#reviewcontainer").width(reviewsContainerWidth + 300)
			}
			
			
			html = "<div class='reviewpanel'>    " +
	   		text +
	   		"</div>"

	   		$("#badreviewspanel").append(html)
		}
				
		
	}
	 /*var config = {    
		     over: showDetailsOnHover(), // function = onMouseOver callback (REQUIRED)    
		     timeout: 00, // number = milliseconds delay before onMouseOut    
		     out: hideDetailsOnHover, // function = onMouseOut callback (REQUIRED)
		     interval: 500
		};
	 */
	 
	 
	 //$(".reviewpanel").hoverIntent(config);
	 
 	
 }
 
 
 function clicked(item) {
	    id=$(item).attr("id");

	    var restid=$("#" + id +"> :first-child").attr("id")

	    var n=restid.split("__");
	    business_id=n[1];

	    window.location = "/wordViz/reviews?id="+business_id;

	}
 

 var timer;
 
function addHoverListeners() {
	

	$('.reviewpanel').live('mouseover', function(event) {
	    
		$Sibling = $(this).siblings(".child");
		var inhtml = $(this).html();
		
		var parentOffset = $(this).parent().offset(); 
		
		timer = window.setTimeout(function() {
	    		
	    	var detailsdiv = $("<div class='popupdiv' id='tooltip_x' class='tooltip_x'>"+ inhtml +"</div>");
		    
   	    	$('body').append(detailsdiv)
	   	    
   	    	//$('#tooltip_x').css({'top':(event.pageY),'left':(event.pageX - 325), 'position':'absolute'});
   	    	$('#tooltip_x').css({'top':(100),'left':(event.pageX - 325), 'position':'absolute'});
	   	    	
	   	    		    	
	    	
	        }, 500);
	});

	$('.reviewpanel').live('mouseout', function(event) {
	    if (timer) {
	        window.clearTimeout(timer);
	    }
	    
	    $('#tooltip_x').remove()

	});
}
 
function resetview() {
	fetchReviewsContainingWord("", business_id)
}
