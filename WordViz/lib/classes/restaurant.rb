class Restaurant

   attr_accessor :name, :address_line1, :address_line2
   
   
   def initialize(name_passed, address_line1, address_line2)
      self.name = name_passed
      self.address_line1 = address_line1 
      self.address_line2 = address_line2
   end
   
end