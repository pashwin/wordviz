class DropTypeFromReviewData < ActiveRecord::Migration
  def up
      remove_column :review_data, :type
  end

  def down
  end
end
