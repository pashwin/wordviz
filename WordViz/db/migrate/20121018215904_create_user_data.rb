class CreateUserData < ActiveRecord::Migration
  def change
    create_table :user_data do |t|
      t.string :user_id
      t.string :name
      t.string :url
      t.float :average_stars
      t.integer :review_count
      t.string :type

      t.timestamps
    end
  end
end
