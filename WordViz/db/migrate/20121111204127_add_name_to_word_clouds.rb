class AddNameToWordClouds < ActiveRecord::Migration
  def change
    add_column :word_clouds, :name, :string
  end
end
