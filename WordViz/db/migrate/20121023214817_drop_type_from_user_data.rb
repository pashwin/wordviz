class DropTypeFromUserData < ActiveRecord::Migration
  def up
    remove_column :user_data, :type
  end

  def down
  end
end
