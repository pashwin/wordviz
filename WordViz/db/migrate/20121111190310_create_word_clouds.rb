class CreateWordClouds < ActiveRecord::Migration
  def change
    create_table :word_clouds do |t|
      t.string :business_id
      t.text :word_count
      t.text :word_reviewids

      t.timestamps
    end
  end
end
